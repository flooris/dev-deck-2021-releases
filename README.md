# Dev Deck 2021 releases #

This repository is responsible for hosting Dev Deck 2021 releases. Releases can be found under the Downloads tab.


## Source code ##
The Dev Deck 2021 source code can be found in the main [dev-deck-2021 repository](https://bitbucket.org/flooris/dev-deck-2021). No code changes should be made here.